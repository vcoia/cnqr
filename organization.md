# Package cnqr

## Objective

Use Composite Nonlinear Quantile Regression (CNQR) to _fit_ and _select_ upper quantile function forecasters that can be queried/assessed/used.

## Concepts

Here are the concepts that the package works with.

* __Forecast__: a (quantile) function over the domain $(\tau_c, 1)$.

* __Forecaster__: a set of forecasts, indexed by some predictor.

* __Model__: This is a set of forecasters, indexed by some parameter.

