---
title: "Investigate Regression Functionality"
output: 
    html_document:
        toc: true
---

```{r}
library(copsupp)
library(cnqr)
library(CopulaModel)
set.seed(2323)
```

We'll investigate two aspects of the functions in the `copsupp`, using simulated data:

1. Whether the functions in the `copsupp` package, under the correct model, gives "correct" output.
2. How good the model-fitting features of the package are.

Regression will be over the entire range of quantiles:

```{r}
tau <- space_taus(10)
```

## Generating Distribution, and Sample

First, we'll specify the joint distribution of `p` predictors and a response. We'll do so through a vine.

```{r}
p <- 4
## Marginals -- we'll keep predictors as Unif(0,1)
FY <- pexp
QY <- qexp
## Vine specification (append "0" to mean truth): 
#### Vine array (put Y at end of array so that we know the order
####  that it links up with predictors)
A0 <- Dvinearray(p+1)
#### Copula families and parameters
copmat0 <- makeuppertri(c("mtcj", "bvtcop", "gum", "bvtcop",
                          "bvncop", "frk", "gum",
                          "indepcop", "joe",
                          "frk"), 4, 5, "")
cparmat0 <- makeuppertri.list(c(2.5, 0.7, 3, 2, 0.5, 2,
                                0.4, 3, 2,
                                2, 
                                2), len = c(1,2,1,2,1,1,1,0,1,1), nrow=4, ncol=5)
```

That's all we need to specify the joint distribution. It'll be helpful later, though, to extract the vine connecting the predictors, as well as the Bayesian Network connecting the response to the predictors.

```{r}
## Extract joint distribution of predictors:
AX0 <- rvinesubset(A0, 1:p)
copmatX0 <- reform.copmat(copmat0, AX0, A0)
cparmatX0 <- reform.copmat(cparmat0, AX0, A0)
## Extract Bayesian Network info:
xord0 <- A0[1:p, p+1]
ycops0 <- copmat0[, p+1]
ycpars0 <- cparmat0[, p+1]
```

Now, simulate the data.

```{r}
n <- 500
dat <- fvinesim(n, A = A0, cops = copmat0, cpars = cparmat0)
y <- QY(dat[, p+1])
xdat <- dat[, 1:p]
## Test set:
dattest <- fvinesim(n, A = A0, cops = copmat0, cpars = cparmat0)
ytest <- QY(dattest[, p+1])
xdattest <- dattest[, 1:p]
```

## Forecaster 0: Full knowledge of DGP (i.e. does code work?)

Make "forecasts" for the training set and the test set, and check out their calibration plots. We'll take a look at them both, even though they have the same expectations, because they act as a control (i.e. if there's a "defect" in a plot, then that defect in fitted forecasters is not a bad thing).

```{r}
Fcond <- pcondseq.vine(xord0, xdat,
                       rvinefit=list(A=truncvarray(AX0, 2), 
                                     copmat=copmatX0[-3, ], 
                                     cparmat=cparmatX0[-3, ]))
yhat <- qcondBN(tau = tau, cop = ycops0, cpar = ycpars0, QY = QY, Fcond = Fcond)
calibration(y, yhat, tau)
Fcond <- pcondseq.vine(xord0, xdattest,
                       rvinefit=list(A=truncvarray(AX0, 2), 
                                     copmat=copmatX0[-3, ], 
                                     cparmat=cparmatX0[-3, ]))
yhattest <- qcondBN(tau = tau, cop = ycops0, cpar = ycpars0, QY = QY, Fcond = Fcond)
calibration(ytest, yhattest, tau)
```

## Forecaster 1: No knowledge of DGP

```{r}
load("~/Documents/ActiveAreas/Canmore-ResearchProject/cnqr/tests/simu-upper.Rdata")
```

We'll use `cnqr()` to fit a forecaster without knowledge of the dependence of the predictors and response (we know the marginals though).

```{r}
# Q1 <- cnqr2(y, xdat, xmargs=identity, FY=FY, QY=QY, tau=tau, verbose=TRUE)
# Q1xdat <- Q1(xdat)
# Q1xdattest <- Q1(xdattest)
```

Assess it via calibration plot on training set and test set:

```{r, echo=FALSE}
calibration(y, Q1xdat, tau = tau)
calibration(ytest, Q1xdattest, tau = tau)
```

## Forecaster 2: Fit with knowledge of predictor distribution

Now we'll add knowledge of the predictor distribution. This is some indication as to how the approach of estimating the sequential conditional distributions performs (it previously fit an entirely new vine every time a new predictor is added)

```{r}
# (fitBN <- fit.BN(y, xdat, ymarg = pexp))
# len <- sapply(fitBN$cparstart, length)
# Fcond <- pcondseq.vine(fitBN$xord, xdat, verbose = TRUE,
#                        rvinefit=list(A=truncvarray(AX0, 2), 
#                                      copmat=copmatX0[-3, ], 
#                                      cparmat=cparmatX0[-3, ]))
# yhat <- function(cparvec){
#     cpar <- cparvec2cpar(cparvec, len)
#     qcondBN(tau = tau, cop = fitBN$cops, cpar = cpar, QY = QY, Fcond = Fcond)
# } 
# obj <- cnqrobj(y, yhat, tau)
# res <- rnlm(obj, c(fitBN$cparstart, recursive=T), in.range = cparspace(fitBN$cops))
# cparhat <- cparvec2cpar(res$estimate, len)
# yhat_new <- function(x) {
#     Fcondnew <- pcondseq.vine(fitBN$xord, x, 
#                               rvinefit=list(A=truncvarray(AX0, 2), 
#                                             copmat=copmatX0[-3, ], 
#                                             cparmat=cparmatX0[-3, ]))
#     qcondBN(tau=tau, cop=fitBN$cops, cpar=cparhat, QY=QY, Fcond=Fcondnew)
# }
# Q2xdat <- yhat(res$estimate)
# Q2xdattest <- yhat_new(xdattest)
```

calibration of training set and test set:

```{r}
calibration(y, Q2xdat, tau)
calibration(ytest, Q2xdattest, tau)
```

## Forecaster 3: Fit with knowledge of Bayesian Network

We'll consider the distributions linking the response and the sequentially conditional predictors as known -- i.e. the Bayesian Network component. The predictor distribution (and hence the sequential cdfs) are unknown and need estimation. But there are two ways to estimate these:

### Estimate `Fcond` with entirely different vines

In this setting, an entirely different vine is fit each time a predictor is added (according to the Bayesian Network). This is also what Forecaster 1 uses.

```{r}
# Fcond <- list(xdat[, xord0[1]])
# fitX <- list(NA)
# for (k in 2:p) {
#     ## Fit a vine to variables ord[1:k]
#     fitX[[k]] <- fit.rvine(xdat, vars = xord0[1:k])
#     ## Get Fconds for the data
#     Fcond[[k]] <- pcondrvine(xdat, xord0[k], fitX[[k]]$A,
#                              fitX[[k]]$copmat, fitX[[k]]$cparmat,
#                              verbose = TRUE)
# }
# Fcond <- do.call(cbind, Fcond)
# 
# yhat <- qcondBN(tau = tau, cop = ycops0, cpar = ycpars0, QY = QY, Fcond = Fcond)
# yhat_new <- function(x) {
#     Fcondnew <- list(x[, xord0[1]])
#     for (k in 2:p) {
#         Fcondnew[[k]] <- pcondrvine(x, xord0[k], fitX[[k]]$A,
#                                  fitX[[k]]$copmat, fitX[[k]]$cparmat)
#     }
#     Fcondnew <- do.call(cbind, Fcondnew)
#     qcondBN(tau=tau, cop=ycops0, cpar=ycpars0, QY=QY, Fcond=Fcondnew)
# }
# Q3xdat <- yhat
# Q3xdattest <- yhat_new(xdattest)
```

calibration of training set and test set:

```{r}
calibration(y, Q3xdat, tau)
calibration(ytest, Q3xdattest, tau)
```

### Estimate `Fcond` by adding predictors as leaves

In this setting, as predictors are added in the Bayesian Network, a joint predictor distibution is formed by sequentially adding new predictors as a leaf in the vine. 

There's currently no functionality for "where's best to put the next predictor", so I'll use the true order (the one that generated the data) -- but I won't tell it that it's 2-truncated.

```{r}
# fitX <- fit.rvine(xdat, A=AX0)
# # shhh <- capture.output(fitX <- VineCopula::RVineCopSelect(xdat, Matrix=AX0[p:1, p:1],))
# Fcond <- pcondseq.vine(xord0, xdat, rvinefit=fitX, verbose=T)
# yhat <- qcondBN(tau = tau, cop = ycops0, cpar = ycpars0, QY = QY, Fcond = Fcond)
# yhat_new <- function(x) {
#     Fcondnew <- pcondseq.vine(xord0, x, rvinefit=fitX)
#     qcondBN(tau=tau, cop=ycops0, cpar=ycpars0, QY=QY, Fcond=Fcondnew)
# }
# Q3xdatb <- yhat
# Q3xdattestb <- yhat_new(xdattest)
```

calibration of training set and test set:

```{r}
calibration(y, Q3xdatb, tau)
calibration(ytest, Q3xdattestb, tau)
```

## Forecaster 4: Fit with all knowledge except cnqr parameters

Here, we'll assume everything about the generating distribution is known, except for the parameters that link the response with the predictors -- those will be estimated by CNQR.

```{r}
# Fcond <- pcondseq.vine(xord0, xdat,
#                        rvinefit=list(A=truncvarray(AX0, 2), 
#                                      copmat=copmatX0[-3, ], 
#                                      cparmat=cparmatX0[-3, ]))
# len <- sapply(ycpars0, length)
# yhat <- function(cparvec){
#     cpar <- cparvec2cpar(cparvec, len)
#     qcondBN(tau = tau, cop = ycops0, cpar = cpar, QY = QY, Fcond = Fcond)
# }
# obj <- cnqrobj(y, yhat, tau)
# (startval <- c(ycpars0, recursive=T))
# (res <- rnlm(obj, startval, in.range = cparspace(ycops0)))
# cparhat <- cparvec2cpar(res$estimate, len)
# yhat_new <- function(x) {
#     Fcondnew <- pcondseq.vine(xord0, x,
#                        rvinefit=list(A=truncvarray(AX0, 2), 
#                                      copmat=copmatX0[-3, ], 
#                                      cparmat=cparmatX0[-3, ]))
#     qcondBN(tau = tau, cop = ycops0, cpar = cparhat, QY = QY, Fcond = Fcondnew)
# }
# Q4xdat <- yhat(res$estimate)
# Q4xdattest <- yhat_new(xdattest)
```

Get calibration plot of training data and test data:

```{r}
calibration(y, Q4xdat, tau = tau)
calibration(ytest, Q4xdattest, tau = tau)
```
